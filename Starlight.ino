// ==================================================================================
// STARLIGHT
// A sketch that controls 3 LEDs and an IR receiver as a kids' night light.
// ==================================================================================
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <IRremote.h>
#include <IRremoteInt.h>

// ==================================================================================
// CONSTANTS
// ==================================================================================
// We need three PWM outputs to create a sparkling starlight
int LED1_PIN = 6;
int LED2_PIN = 5;
int LED3_PIN = 9;

// We need one input PIN to get information from the IR receiver
int IR_PIN = 8;

// Constants defining the keys for the IR remote
unsigned long ONOFF = 0x01FE7887;
unsigned long ENTER = 0x01FE40BF;
unsigned long UP = 0x01FE58A7;
unsigned long DOWN = 0x01FEA05F;
unsigned long REPT = 0xFFFFFFFF;

// We have a loop delay for each of the LEDs
unsigned long LED1_DELAY = 40;
unsigned long LED2_DELAY = 35;
unsigned long LED3_DELAY = 30;

// We also have certain ranges in which the LEDs sparkle
int LED1_SPARKLE_MIN = 100;
int LED1_SPARKLE_MAX = 255;
int LED2_SPARKLE_MIN = 140;
int LED2_SPARKLE_MAX = 255;
int LED3_SPARKLE_MIN = 60;
int LED3_SPARKLE_MAX = 255;

// Our program is able to run in 3 modes: 0 = OFF, 1 = sparkle, 2 = constant light
int OFF = 0;
int SPARKLE = 1;
int CONSTANT = 2;

// ==================================================================================
// VARIABLES
// ==================================================================================
int mode;
int lastModeBeforeOff;
int led1Value;
int led2Value;
int led3Value;
int led1Increment;
int led2Increment;
int led3Increment;
unsigned long led1LastMillis;
unsigned long led2LastMillis;
unsigned long led3LastMillis;
unsigned long lastIRCommand = 0;
unsigned long watchDogLastMillis = 0;

IRrecv irrecv(IR_PIN);
decode_results results;

// ==================================================================================
// WATCHDOG INTERRUPT HANDLER
// Called when the watchdog interrupt fires
// ==================================================================================
ISR(WDT_vect) 
{
   wdt_disable();
}


// ==================================================================================
// SLEEP WITH WATCHDOG
// Puts the processor to sleep, but wakes up after 500ms to check for IR input.
// ==================================================================================
void sleepWithWatchDog()
{
  // disable ADC
  ADCSRA = 0;  

  // clear various "reset" flags
  MCUSR = 0;     
  
  // allow changes, disable reset
  WDTCSR = bit (WDCE) | bit (WDE);
  
  // set interrupt mode and an interval 
  WDTCSR = bit (WDIE) | bit (WDP2) | bit (WDP0);    // set WDIE, and half second delay
  
  wdt_reset();  // pat the dog
  
  set_sleep_mode (SLEEP_MODE_PWR_DOWN);  
  sleep_enable();
 
  // turn off brown-out enable in software
  MCUCR = bit (BODS) | bit (BODSE);
  MCUCR = bit (BODS); 
  sleep_cpu ();  
  
  // cancel sleep as a precaution
  sleep_disable();
  
  // remember the last time we woke up, so we can idle for 0.5 secs
  watchDogLastMillis = millis();
  
  // Let the IR receiver receive stuff
  irrecv.resume();
}

void setup() 
{
  Serial.begin(9600);
  
  // Start the IR receiver
  irrecv.enableIRIn();

  // Set the LED pins as OUTPUT and turn them off
  pinMode(LED1_PIN, OUTPUT);      
  pinMode(LED2_PIN, OUTPUT);      
  pinMode(LED3_PIN, OUTPUT);      
  
  analogWrite(LED1_PIN, 0);
  analogWrite(LED2_PIN, 0);  
  analogWrite(LED3_PIN, 0);
  
  mode = SPARKLE;
  led1Value = LED1_SPARKLE_MIN;
  led2Value = LED2_SPARKLE_MAX;
  led3Value = LED3_SPARKLE_MIN + (LED3_SPARKLE_MAX - LED3_SPARKLE_MIN) / 2;
  
  led1Increment = 4;
  led2Increment = -4;
  led3Increment = 4;
  
  led1LastMillis = 0;
  led2LastMillis = 0;
  led3LastMillis = 0;
}

void loop()
{
  // Try to decode something from the IR receiver
  if (irrecv.decode(&results))
  {    
    // The "ON/OFF" key was pressed
    if (results.value == ONOFF)
    {
      lastIRCommand = results.value;
      watchDogLastMillis = 0;

      if (mode == OFF)
      {
        mode = lastModeBeforeOff;
      }
      else
      {
        lastModeBeforeOff = mode;
        mode = OFF;

        analogWrite(LED1_PIN, 0);
        analogWrite(LED2_PIN, 0);
        analogWrite(LED3_PIN, 0);
        
        // Go to sleep for a second
        sleepWithWatchDog();
      }
    }
    
    // The "ENTER" key was pressed to switch between the two modes
    else if (mode != OFF && results.value == ENTER)
    {
      lastIRCommand = results.value;
      if (mode == SPARKLE)
      {
        mode = CONSTANT;
      }
      else
      {
        mode = SPARKLE;
      }
    }
    
    // The "UP" key was pressed or repeated
    else if (mode != OFF && (results.value == UP || (lastIRCommand == UP && results.value == REPT)))
    {
      lastIRCommand = UP;
      led1Value += 4;
      led1Value = max(0, min(255, led1Value));
    }
    
    // The "DOWN" key was pressed or repeated
    else if (mode != OFF && (results.value == DOWN || (lastIRCommand == DOWN && results.value == REPT)))
    {
      lastIRCommand = DOWN;
      led1Value -= 4;
      led1Value = max(0, min(255, led1Value));
    }
    
    else
    {
      lastIRCommand = 0;
    }
    
    if (mode != OFF)
    {
      // Receive the next value
      irrecv.resume();      
    }
  }

  // If we're in OFF mode, we put the PIC to sleep for half a second
  if (mode == OFF && watchDogLastMillis > 0 && (millis() - watchDogLastMillis >= 500))
  {
    sleepWithWatchDog();
  }
  
  // The mode says we should show a constant brightness
  if (mode == CONSTANT)
  {
      analogWrite(LED1_PIN, led1Value);
      analogWrite(LED2_PIN, led1Value);
      analogWrite(LED3_PIN, led1Value);
  }
  
  // The mode says we should sparkle
  if (mode == SPARKLE)
  {
    unsigned long currentMillis = millis();
    
    // Should LED 1 change?
    if (currentMillis - led1LastMillis > LED1_DELAY)
    {
      if (led1Value <= LED1_SPARKLE_MIN)
        led1Increment = 4;
      if (led1Value >= LED1_SPARKLE_MAX)
        led1Increment = -4;
      
      led1Value += led1Increment;
      led1Value = max(LED1_SPARKLE_MIN, min(LED1_SPARKLE_MAX, led1Value));
      
      led1LastMillis = currentMillis;
      analogWrite(LED1_PIN, led1Value);
    }

    // Should LED 2 change?
    if (currentMillis - led2LastMillis > LED2_DELAY)
    {
      if (led2Value <= LED2_SPARKLE_MIN)
        led2Increment = 4;
      if (led2Value >= LED2_SPARKLE_MAX)
        led2Increment = -4;
      
      led2Value += led2Increment;
      led2Value = max(LED2_SPARKLE_MIN, min(LED2_SPARKLE_MAX, led2Value));

      led2LastMillis = currentMillis;
      analogWrite(LED2_PIN, led2Value);
    }

    // Should LED 3 change?
    if (currentMillis - led3LastMillis > LED3_DELAY)
    {
      if (led3Value <= LED3_SPARKLE_MIN)
        led3Increment = 4;
      if (led3Value >= LED3_SPARKLE_MAX)
        led3Increment = -4;
      
      led3Value += led3Increment;
      led3Value = max(LED3_SPARKLE_MIN, min(LED3_SPARKLE_MAX, led3Value));
    
      led3LastMillis = currentMillis;
      analogWrite(LED3_PIN, led3Value);
    }
  }
}

